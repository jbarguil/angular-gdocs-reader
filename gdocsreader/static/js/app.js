var app = angular.module('spreadsheetApp', []);

app.controller('SpreadsheetsController', function($scope, $timeout) {
    var errorPromise,
        ERROR_TIMEOUT = 5000;   // in millis

    $scope.URL_CARROS = 'https://docs.google.com/spreadsheets/d/13YmG1d_JKzYxnKyeDcILUGKGBakGjUdiRAHF47P0FnQ/pubhtml';
    $scope.URL_COPA = 'https://docs.google.com/spreadsheets/d/1wuf6QJw7jGtG6RNhHRZIkW19XOhrh0V7deDF5ZtHiy8/pubhtml';
    $scope.URL_TELS = 'https://docs.google.com/spreadsheets/d/19ndEHFbehedreGhE-b2tgq85P4eX5JcZECTQGjbAIRk/pubhtml';
    $scope.url = $scope.URL_TELS;

    angular.element(document).ready(function() {
        $scope.loadValues();
        $scope.viewSpsTabs = true;
        $scope.viewRowTable = true;
        $scope.kind = "tabs";
        $scope.subkind = "table";
    });

    $scope.loadValues = function() {
        $scope.models = {};
        $scope.modelNames = [];
        $scope.dataLoaded = false;
        $scope.error = false;

        try {
            Tabletop.init({
                key: $scope.url,
                callback: showInfo,
                simpleSheet: true
            });
        } catch (err) {
            setError();
        }

        errorPromise = $timeout(setError, ERROR_TIMEOUT);
    };

    $scope.toggleSpsVisualization = function() {
        $scope.viewSpsTabs = !$scope.viewSpsTabs;
        $scope.kind = $scope.kind === "tabs" ? "accordions" : "tabs";
    }

    $scope.toggleRowVisualization = function() {
        $scope.viewRowTable = !$scope.viewRowTable;
        $scope.subkind = $scope.subkind === "table" ? "accordions" : "table";
    }

    var setError = function() {
        $scope.error = true;
        $scope.dataLoaded = true;
    };

    var showInfo = function (data, tabletop) {
        $scope.$apply(function() {
            $timeout.cancel(errorPromise);
            $scope.models = tabletop.models;
            $scope.modelNames = tabletop.model_names.sort();
            $scope.dataLoaded = true;
        });
    };
});

/*
    This directive defines its template dinamically.
    It receives a 'kind' argument that is included into the URL of a resource that is dinamically
    loaded by the ng-include directive.
*/
app.directive('jbSpreadsheetView', function () {
    return {
        restrict: 'A',
        replace: true,
        scope: {
            spreadsheets: '=',
            names: '=',
            kind: '=',
            subkind: '='
        },
        link: function(scope, element, attrs) {
            scope.contentUrl = 'views/spreadsheet-' + scope.kind + '.html';
            scope.$watch('kind', function(k) {
                scope.contentUrl = 'views/spreadsheet-' + k + '.html';
            });
        },
        template: '<div ng-include="contentUrl"></div>'
    };
});

// This directive also defines the template URL dinamically.
app.directive('jbDataRows', function () {
    return {
        restrict: 'A',
        replace: true,
        scope: {
            spreadsheet: '=',
            spsName: '@',
            kind: '='
        },
        link: function(scope, element, attrs) {
            scope.contentUrl = 'views/data-rows-' + scope.kind + '.html';
            scope.$watch('kind', function (k) {
                scope.contentUrl = 'views/data-rows-' + k + '.html';
            });
        },
        template: '<div ng-include="contentUrl"></div>'
    };
});

app.directive('jbDataEntry', function () {
    return {
        restrict: 'A',
        replace: true,
        scope: {
            columnName: '=',
            data: '='
        },
        link: function (scope, element, attrs) {
            scope.isLink = function (name) {
                return name === "link";
            };
        },
        templateUrl: 'views/data-entry.html'
    };
});

// This is stupid, it's just so we move stuff to another file to not clutter index.html.
app.directive('jbUrlSelectionModal', function () {
    return {
        restrict: 'A',
        replace: true,
        templateUrl: 'views/url-selection-modal.html'
    };
});

app.filter('validHtmlId', function () {
    return function(item) {
        // Removes any non-word characters from the string.
        return item.replace(/\W/g, '');
    }
});
