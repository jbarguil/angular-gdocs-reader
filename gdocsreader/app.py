# -*- coding: utf-8; -*-
import flask
import os


app = flask.Flask(__name__, static_url_path="")


@app.route("/<path:path>", methods=["GET", "POST"])
def static_proxy(path):
    # send_static_file will guess the correct MIME type
    return app.send_static_file(os.path.join("", path))


if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    debug = port == 5000

    app.run(host="0.0.0.0", port=port, debug=debug)
