# Angular Gdocs Reader #

Toy project for learning AngularJS. 

### What is it? ###

It's a single webpage that fetches data from a public Google Spreadsheet and displays it in the page. The data source can be dynamically changed, as well as its UI.

###[See it live here](https://gdocsreader.herokuapp.com/index.html).###