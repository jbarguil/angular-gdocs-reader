import setuptools

setuptools.setup(
    name="gdocsreader",
    version="0.1dev",
    author="Joao Marcos Barguil",
    author_email="jbarguil@gmail.com",
    packages=["gdocsreader"],
    install_requires=[
        "flask",
    ]
)
